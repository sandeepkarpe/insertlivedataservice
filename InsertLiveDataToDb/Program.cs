﻿using SignalATM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InsertLiveDataToDb
{
    class Program
    {
        SQLDataAccess sqlDataAccess;
        Hashtable inputParams;

        static void Main(string[] args)
        {
            GetStartTrueFXDataAsync();
        }

        private static void GetStartTrueFXDataAsync()
        {
            Program program = new Program();
            HttpClient client;
            string url = "https://webrates.truefx.com/rates/connect.html?f=csv";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));             
            HttpResponseMessage response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                string responseData = response.Content.ReadAsStringAsync().Result;
                if (responseData != string.Empty && responseData != null)
                {
                    program.InsertDataIntoDB(responseData);
                }
            }
           // return View();
        }

        private void InsertDataIntoDB(string responseData)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            DataTable mstInstrumentDT = new DataTable();
            DataTable dt2 = new DataTable();
            try
            {
                if (responseData != null)
                {
                    string[] pairsArray = responseData.Split(new Char[] { '\n' });
                    for (int i = 0; i < pairsArray.Length; i++)
                    {
                        string pairList = pairsArray[i].Replace("/", "-");
                        string[] pairData = Regex.Split(pairList, ",");
                        string instrumentName = pairData[0];
                        int resultIns = InsertInstrumentName(instrumentName);
                        if (resultIns == 1)
                        {
                            int instrumentId = GetInstrumentId(instrumentName);
                            inputParams.Clear();
                            if (pairsArray[i] != "\r" && pairsArray[i] != "" && instrumentId != 0)
                            {

                                decimal PriceBid = Convert.ToDecimal(pairData[2] + pairData[3]);
                                decimal PriceAsk = Convert.ToDecimal(pairData[4] + pairData[5]);
                                decimal PriceMid = Convert.ToDecimal((PriceBid + PriceAsk) / 2);
                                DateTime EntryDate = DateTime.UtcNow;

                                inputParams.Add("@InstrumentId", instrumentId);
                                inputParams.Add("@InstrumentName", instrumentName);
                                inputParams.Add("@PriceBid", PriceBid);
                                inputParams.Add("@PriceAsk", PriceAsk);
                                inputParams.Add("@PriceMid", PriceMid);
                                inputParams.Add("@LiveFeed", "True");
                                inputParams.Add("@LiveFeedSourceName", "TrueFX");
                                inputParams.Add("@EntryDate", EntryDate);

                                sqlDataAccess.ExecuteStoreProcedure("usp_Set_InstrumentPriceLive", inputParams);
                                inputParams.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        private int GetInstrumentId(string instrumentName)
        {
            int instrumnetId = 0;
            DataTable mstInstrumentDT = new DataTable();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@InstrumentName", instrumentName);
            try
            {

                var instrId = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_Get_InstrumentId]", inputParams);
                instrumnetId = instrId.Rows[0].Field<int>(0); ;

            }
            catch (Exception ex)
            {

                throw;
            }
            return instrumnetId;
        }

        private int InsertInstrumentName(string instrumentName)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@InstrumentName", instrumentName);
                inputParams.Add("@EntryDate", DateTime.UtcNow.ToString());
                sqlDataAccess.ExecuteStoreProcedure("usp_Set_InstrumentName", inputParams);
                inputParams.Clear();
                return 1;
            }
            catch (Exception EX)
            {
                return 0;
            }
            //throw new NotImplementedException();
        }
    }
}
